const express = require('express')
const dotenv = require('dotenv')
dotenv.config();

const app = express()
app.get('/ping', (req, res) => res.send("pong"));
app.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}...`));